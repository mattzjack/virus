import requests, bs4, datetime, os

print(datetime.datetime.now().date())

pa_health_url = 'https://www.health.pa.gov/topics/disease/coronavirus/Pages/Cases.aspx'
wi_url = 'https://www.dhs.wisconsin.gov/covid-19/county.htm'
cdc_url = 'https://www.cdc.gov/coronavirus/2019-ncov/cases-updates/cases-in-us.html'
data_file = './data.csv'

pa_response = requests.get(pa_health_url)
pa_soup = bs4.BeautifulSoup(pa_response.text, 'html.parser')
# ugh, no id whatsoever
pa_tables = pa_soup.find_all('table')
pa_counties_table = pa_tables[4]
# still not stable!!!
counties_table_title = pa_counties_table.find_previous('h4').get_text().strip()
assert(counties_table_title == 'County Case Counts to Date')
counties_table_positive = pa_counties_table.find_all('tr')[0].find_all('td')[1].get_text().strip()
assert(counties_table_positive == 'Total Cases')
allegheny_data = pa_counties_table.find_all('tr')[2].find_all('td')[:2]
assert(allegheny_data[0].get_text() == 'Allegheny')
allegheny_today_num = int(allegheny_data[1].get_text())
print('Allegheny:', allegheny_today_num)

pa_data = pa_tables[0].find_all('tr')
assert(pa_data[0].find_all('td')[0].get_text().strip() == 'Total Cases*')
pa_today_num = int(''.join([c for c in pa_data[1].find_all('td')[0].get_text() if c.isdigit()]))
print('PA:', pa_today_num)

wi_response = requests.get(wi_url)
wi_soup = bs4.BeautifulSoup(wi_response.text, 'html.parser')
wi_table = wi_soup.find(id='covid-county-table')
print(wi_table)
assert(False)

cdc_response = requests.get(cdc_url)
cdc_soup = bs4.BeautifulSoup(cdc_response.text, 'html.parser')
cdc_counts = cdc_soup.find_all(class_='count')
guess = cdc_counts[0]
assert(guess.parent.get_text().strip().startswith('Total Cases\n'))
us_today_num = int(''.join([c for c in guess.get_text() if c.isdigit()]))
print('USA:', us_today_num)

prev_size = os.path.getsize(data_file)
i_should_write = True
need_newline = True
with open(data_file, 'r') as f:
    f.seek(max(0, prev_size - 100), 0)
    previous = f.readlines()[-1]
    if previous.endswith('\n'):
        need_newline = False
    prev_date, prev_alle, prev_pa, prev_usa = previous.strip().split(',')
    if prev_date == str(datetime.datetime.now().date()):
        print('you already have an entry! update it yourself')
        i_should_write = False
    if int(prev_alle) == allegheny_today_num:
        print("looks like allegheny didn't update )-: try again later")
        i_should_write = False
    if int(prev_pa) == pa_today_num:
        print("looks like PA didn't update )-: try again later")
        i_should_write = False
    if int(prev_usa) == us_today_num:
        print("looks like CDC didn't update )-: try again later")
        i_should_write = False

if i_should_write:
    with open(data_file, 'a') as f:
        if need_newline:
            f.write('\n')
        f.write(f'{datetime.datetime.now().date()},{allegheny_today_num},{pa_today_num},{us_today_num}')






