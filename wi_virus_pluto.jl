### A Pluto.jl notebook ###
# v0.12.11

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ ba236dc4-0de1-11eb-3037-1d9d1fed8d48
begin
	using HTTP, Dates, DataFrames, StatsPlots, Indicators, Pipe
	import JSON3, PlutoUI
end

# ╔═╡ f157b2f4-0de7-11eb-1de5-431ebbf98155
md"# ignore rest"

# ╔═╡ eb1671c8-2e08-11eb-096a-e54d96cfc5a9
function get_df(url)
	df = @pipe url |> HTTP.get |> _.body |> String |> JSON3.read |> _.features |>
		getindex.(_, :attributes) |> DataFrame |> something.(_, NaN16) |> sort!(_, :DATE)
	df.DATE = df.DATE ./ 1000 .|> Dates.unix2datetime
	df.RATE_7DAYAVG = sma(df.POS_NEW ./ df.TEST_NEW)
	df
end

# ╔═╡ 517949ae-0de8-11eb-29f1-0d441aebf4c4
begin	
	dane_url = "https://dhsgis.wi.gov/server/rest/services/DHS_COVID19/COVID19_WI/MapServer/12/query?where=NAME%20%3D%20%27DANE%27&outFields=DATE,POS_NEW,POS_7DAYAVG,TEST_NEW&outSR=4326&f=json"
	wi_url = "https://dhsgis.wi.gov/server/rest/services/DHS_COVID19/COVID19_WI/MapServer/11/query?where=1%3D1&outFields=DATE,POS_NEW,POS_7DAYAVG,DTH_NEW,DTH_7DAYAVG,TEST_NEW&outSR=4326&f=json"
	dane_full_df = get_df(dane_url)
	wi_full_df = get_df(wi_url)
end;

# ╔═╡ 181e47ee-0de4-11eb-3317-3dd17340ae94
@bind dane_pos_days PlutoUI.Slider(30:nrow(dane_full_df))

# ╔═╡ a3780462-0de4-11eb-0f2d-3d7f2a4ae9aa
@bind dane_rate_days PlutoUI.Slider(30:nrow(dane_full_df))

# ╔═╡ 2981f264-0de5-11eb-30da-bb8f0497360e
@bind wi_pos_days PlutoUI.Slider(10:nrow(wi_full_df))

# ╔═╡ 28b5bc80-0de5-11eb-0ce7-7be8aa352e1f
@bind wi_rate_days PlutoUI.Slider(30:nrow(wi_full_df))

# ╔═╡ 3970614c-0de5-11eb-18a2-49bf0412e94e
@bind wi_death_days PlutoUI.Slider(30:nrow(wi_full_df))

# ╔═╡ fc4f96e2-0de5-11eb-0989-8b0c6ce4a416
dane_pos_df = last(dane_full_df, dane_pos_days);

# ╔═╡ 865ada44-0de2-11eb-1dee-b7ea017ea07c
begin
	theme(:bright)
	@df dane_pos_df bar(:DATE, :POS_NEW, ylim = (0, Inf), label = "daily new positives", title = "Dane new positives", legend = :topleft, linecolor = :match, linealpha = 0.2, fillalpha = 0.2)
	@df dane_pos_df plot!(:DATE, :POS_7DAYAVG, linewidth = 3, label = "weekly moving average")
end

# ╔═╡ fc30ca02-0de5-11eb-1291-491d62f6c765
dane_rate_df = last(dane_full_df, dane_rate_days);

# ╔═╡ 0520451c-0de3-11eb-2441-d784d524226c
begin
	theme(:bright)
	@df dane_rate_df plot(:DATE, :POS_NEW ./ :TEST_NEW, label = nothing, title = "Dane positive rate", linewidth = 3, legend = :topleft, ylim = (0, 1), linealpha = 0.2)
	@df dane_rate_df plot!(:DATE, :RATE_7DAYAVG, label = "weekly moving average", linewidth = 3)
end

# ╔═╡ fc070788-0de5-11eb-1a63-8158e6ef5cc5
wi_pos_df = last(wi_full_df, wi_pos_days);

# ╔═╡ a61b62fe-0de5-11eb-267d-9718ff0c6c52
begin
	theme(:bright)
	@df wi_pos_df bar(:DATE, :POS_NEW, ylim = (0, Inf), label = "daily new positives", title = "WI new positives", legend = :topleft, linecolor = :match, fillalpha = 0.2, linealpha = 0.2)
	@df wi_pos_df plot!(:DATE, :POS_7DAYAVG, linewidth = 3, label = "weekly moving average")
end

# ╔═╡ fbc9e77c-0de5-11eb-3f02-7dc469a7105d
wi_rate_df = last(wi_full_df, wi_rate_days);

# ╔═╡ c4efbfcc-0de5-11eb-247b-d34e370502ce
begin
	theme(:bright)
	@df wi_rate_df plot(:DATE, :POS_NEW ./ :TEST_NEW, title = "WI positive rate", linewidth = 3, label = nothing, legend = :topleft, ylim = (0, 1), linealpha = 0.2)
	@df wi_rate_df plot!(:DATE, :RATE_7DAYAVG, label = "weekly moving average", linewidth = 3)
end

# ╔═╡ fb8dd64c-0de5-11eb-2f2d-6df52e1a3135
wi_death_df = last(wi_full_df, wi_death_days);

# ╔═╡ d451b7ce-0de5-11eb-3bd1-5d24ee48f856
begin
	theme(:wong2)
	@df wi_death_df bar(:DATE, :DTH_NEW, title = "WI new deaths", label = "daily new deaths", ylim = (0, Inf), legend = :topleft)
	@df wi_death_df plot!(:DATE, :DTH_7DAYAVG, linewidth = 3, label = "weekly moving average")
end

# ╔═╡ Cell order:
# ╠═865ada44-0de2-11eb-1dee-b7ea017ea07c
# ╠═181e47ee-0de4-11eb-3317-3dd17340ae94
# ╠═0520451c-0de3-11eb-2441-d784d524226c
# ╠═a3780462-0de4-11eb-0f2d-3d7f2a4ae9aa
# ╠═a61b62fe-0de5-11eb-267d-9718ff0c6c52
# ╠═2981f264-0de5-11eb-30da-bb8f0497360e
# ╠═c4efbfcc-0de5-11eb-247b-d34e370502ce
# ╠═28b5bc80-0de5-11eb-0ce7-7be8aa352e1f
# ╠═d451b7ce-0de5-11eb-3bd1-5d24ee48f856
# ╠═3970614c-0de5-11eb-18a2-49bf0412e94e
# ╠═f157b2f4-0de7-11eb-1de5-431ebbf98155
# ╠═fc4f96e2-0de5-11eb-0989-8b0c6ce4a416
# ╠═fc30ca02-0de5-11eb-1291-491d62f6c765
# ╠═fc070788-0de5-11eb-1a63-8158e6ef5cc5
# ╠═fbc9e77c-0de5-11eb-3f02-7dc469a7105d
# ╠═fb8dd64c-0de5-11eb-2f2d-6df52e1a3135
# ╠═eb1671c8-2e08-11eb-096a-e54d96cfc5a9
# ╠═517949ae-0de8-11eb-29f1-0d441aebf4c4
# ╠═ba236dc4-0de1-11eb-3037-1d9d1fed8d48
